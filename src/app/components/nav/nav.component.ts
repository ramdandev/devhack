import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  toggle = (e: any) => {
    e.target.classList.toggle("active")
    document.querySelector(".sidebar-ct")?.classList.toggle("active")
  }

}
