import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  data = {
    balance: 2345,
    product: 5,
    project: 5,
    visitor: 578
  }

  constructor() {}

  ngOnInit(): void {
  }

}
