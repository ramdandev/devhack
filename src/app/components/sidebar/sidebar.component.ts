import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    const slink = document.querySelectorAll(".side-link")
    slink.forEach(el => {
      el.addEventListener("click", (e: any) => {
        let sdbar = document.querySelector(".sidebar-ct")?.classList
        if (sdbar?.contains("active")) {
          sdbar.remove("active")
        }
      })
    })
  }

}
