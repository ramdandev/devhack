import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  items = [
    {
      "item-title": "Apple",
      "item-icon": "uil uil-apple",
      "item-desc": [
        "Login catcher",
        "Auto blocker",
        "Fake News page",
        "Bank Card catcher"
      ],
      "item-price": "20"
    },
    {
      "item-title": "Chase bank",
      "item-icon": "uil uil-university",
      "item-desc": [
        "Login catcher",
        "Auto blocker",
        "Fake News page",
        "Bank Card catcher"
      ],
      "item-price": "25"
    },
    {
      "item-title": "PayPal",
      "item-icon": "uil uil-paypal",
      "item-desc": [
        "Login catcher",
        "Auto blocker",
        "Fake News page",
        "Bank Card catcher"
      ],
      "item-price": "20"
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
